(function() {
'use strict';

// Open the menu
$('div.header-right').find('a').on('click', function(e) {
	e.preventDefault();

	$('div.main-menu').slideDown('slow');
});

// Close the menu
$('div.main-menu .header-right').find('a').on('click', function(e) {
	e.preventDefault();

	$('div.main-menu').slideUp('slow');
});

})();