var gulp = require('gulp'),
	scss = require('gulp-ruby-sass'),
	livereload = require('gulp-livereload'),
	uglify = require('gulp-uglify');

var dist = 'build',
	htmlSrc = ['./*.html', './development/modal/*.html'];

// HTML
gulp.task('html', function() {
	gulp.src(htmlSrc)
		.pipe(livereload());
});

// SCSS
gulp.task('scss', function() {
	return scss('css/sass/*.scss', {
		style: 'compressed'
	})
		.pipe(gulp.dest(dist + '/css'))
		.pipe(livereload());
});

// CSS
gulp.task('css', function() {
	gulp.src(['css/*.css', 'css/**/*.css'])
		.pipe(gulp.dest(dist + '/css'))
		.pipe(livereload());
});

var jsSrc = ['js/*.js', 'js/**/*.js'];

// JS
gulp.task('js', function() {
	gulp.src(jsSrc)
		.pipe(uglify())
		.pipe(gulp.dest(dist + '/js'))
		.pipe(livereload());
});

// Watch
gulp.task('watch', function() {
	livereload.listen();
	gulp.watch(htmlSrc, ['html']);
	// gulp.watch('./css/sass/*.scss', ['scss']);
	gulp.watch(['css/*.css', 'css/**/*.css'], ['css']);
	gulp.watch(jsSrc, ['js']);
});

// Default
gulp.task('default', ['html', 'css', 'js', 'watch']);